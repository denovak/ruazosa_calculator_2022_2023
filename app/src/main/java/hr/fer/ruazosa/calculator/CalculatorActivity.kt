package hr.fer.ruazosa.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import hr.fer.ruazosa.calculator.databinding.ActivityCalculatorBinding

class CalculatorActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCalculatorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCalculatorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var inOperatorMode: Boolean = false
        var inResultMode: Boolean = false



        binding.resetButtonId.setOnClickListener {
            Calculator.reset()
            binding.resultTextViewId.setText("0")
        }

        binding.commaButtonId.setOnClickListener {
            if (!binding.resultTextViewId.text.toString().contains(char = '.')) {
                binding.resultTextViewId.text = binding.resultTextViewId.text.toString() + ".";
            }
        }

        val numericButtonClicked = {view: View ->
            val buttonValue = (view as Button).text.toString()
            if (!binding.resultTextViewId.text.toString().equals("0") && !inOperatorMode && !inResultMode) {
                binding.resultTextViewId.text = binding.resultTextViewId.text.toString() + buttonValue
            }
            else {
                inOperatorMode = false
                inResultMode = false
                binding.resultTextViewId.text = buttonValue
            }
        }

        val operatorButtonClicked = {view: View ->
            val buttonValue = (view as Button).text.toString()
            Calculator.addNumber(binding.resultTextViewId.text.toString())
            Calculator.addOperator(buttonValue)
            inOperatorMode = true
        }



        binding.zeroButtonId.setOnClickListener(numericButtonClicked)
        binding.oneButtonId.setOnClickListener(numericButtonClicked)
        binding.twoButtonId.setOnClickListener(numericButtonClicked)
        binding.threeButtonId.setOnClickListener(numericButtonClicked)
        binding.fourButtonId.setOnClickListener(numericButtonClicked)
        binding.fiveButtonId.setOnClickListener(numericButtonClicked)
        binding.sixButtonId.setOnClickListener(numericButtonClicked)
        binding.sevenButtonId.setOnClickListener(numericButtonClicked)
        binding.eightButtonId.setOnClickListener(numericButtonClicked)
        binding.nineButtonId.setOnClickListener(numericButtonClicked)

        binding.plusButtonId.setOnClickListener(operatorButtonClicked)
        binding.minusButtonId.setOnClickListener(operatorButtonClicked)

        binding.evaluateButtonId.setOnClickListener {
            if (inOperatorMode) {
                Calculator.addNumber("0")
                inOperatorMode = false
            }
            else {
                Calculator.addNumber(binding.resultTextViewId.text.toString())
            }
            Calculator.evaluate()
            binding.resultTextViewId.text = Calculator.result.toString()
            inResultMode = true
            Calculator.reset()
        }


    }
}